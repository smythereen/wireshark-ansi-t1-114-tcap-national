/* packet-t1_114.c
 *
 * Routines for T1.114 ANSI TCAP
 *            & T1.667 ANSI Inteligent Network (AIN) or (IN)
 *            dissection
 *
 * Author: John Smythe <John.Smythe.jr@windstream.com>
 *
 * $Id$
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include	<epan/packet.h>
#include	<epan/prefs.h>
#include	<string.h>

//#define T1_114_SUBTREE          x
#define	NETWORK_BYTE_ORDER	FALSE
#define	MAX_GUI_BUFFER		1024
#define	MAX_ELEMENTS		256

static	        int			proto_t1_114		= -1;
static	        gint			ett_t1_114              = -1;
static	        int			hf_seq			= -1;

static	        gboolean		global_t1_114_names	= TRUE;
static	        gboolean		global_t1_114_expand    = TRUE;
static	const	guint8		*	bytes;
static		tvbuff_t	*	TVB;
static		packet_info	*	PINFO;
static		gint			depth;
static		gint32			offset;
static		gint32			remainder;

static	        module_t        *       t1_114_module   = NULL;
static          dissector_handle_t      t1_114_handle   = NULL;
static		proto_tree      *       t1_114_tree     = NULL;
static          dissector_handle_t      sccp_handle     = NULL;

typedef	struct	cp_s	{
	guint32		offset;
	guint32		id;			// Constructor / Primative ID
	guint32		len;			// Constructor / Primative Length
	guint8		id_len;			// size of id field
	guint8		len_len;		// size of len field
	guint8		construct;		// ( See mtype  Below )
	guint8		class;			// ( See mclass Below )
	guint8		id_raw[ 8 ];		// Constructor / Primative ID Raw Data
	guint8		len_raw[ 6 ];		// Constructor / Primative Length Raw Data
	guint8		p_raw[ 256 ];		// Primative Raw Data
}	cp_t;

typedef	struct	identifier_s	{
	guint8	code		: 5 ;
	guint8	construct	: 1 ;
	guint8	class		: 2 ;
}	identifier_t;

typedef	enum	{
	UNIVERSAL	= 0,
	APPLICATION	= 1,
	CONTEXT		= 2,
	PRIVATE		= 3,
	NATIONAL	= 4,
	MAX_CLASS
}	e_mclass;

static	char	*	mclass[ ] = {
		"Universal  ",		// 0
		"Application",		// 1
		"Context    ",		// 2
		"Private    ",		// 3
		"National   ",		// 4
		NULL
};

typedef	enum	{
	PRIMATIVE	= 0,
	CONSTRUCT	= 1,
	MAX_TYPE
}	e_mtype;

static	char	*	mtype[ ] = {
		"Primative",
		"Construct",
		NULL
};

typedef	struct	item_s	{
	char	*	name;
	gint	(*	func)( char * n, cp_t * cp, char * b );
}	item_t;

static	item_t *	n_matrix[ MAX_CLASS ][ MAX_TYPE ];
static	item_t		Context_Primative[ MAX_ELEMENTS ];
static	item_t		Context_Construct[ MAX_ELEMENTS ];
static	item_t		AIN_Context_Primative[ MAX_ELEMENTS ];

#ifdef	JUNK
static	item_t		AIN_Context_Construct[ MAX_ELEMENTS ];
#endif

#define check_col( a, b )   (0)

static	guint8   BCD_digit( guint8 * bcd, gint dig_num )
{
        return( (guint8)( ( bcd[ dig_num >> 1 ] >> ( ( dig_num & 1 ) ? 4 : 0 ) ) & 0x0F ) );
}

static	void	BCD2Ascii( guint8 * bcd, char ** ascii, int start, gint count )
{
	gint	i;

	for( i = 0; i < count; i++ ) {
		**ascii = "0123456789ABCDEF"[ BCD_digit( bcd, ( start + i ) ) ];
		*ascii += 1;
	}
	**ascii = 0;
}

static	gint	Decimal( char * n, cp_t * cp, char * b )
{
	gint	i;
	guint32	x;

	i = 0;
	x = 0;
	switch( cp->len ) {
	case 4:
		x |= cp->p_raw[ i++ ];
		x <<= 8;
	case 3:
		x |= cp->p_raw[ i++ ];
		x <<= 8;
	case 2:
		x |= cp->p_raw[ i++ ];
		x <<= 8;
	case 1:
		x |= cp->p_raw[ i ];
		break;
	default:
		return( 0 );	// Failure
	}
	g_snprintf(
		b,
		MAX_GUI_BUFFER,
		"%-29s= %u",
		n,
		x
	);
	return( 1 );
}

static	gint	Tran_ID( char * n, cp_t * cp, char * b )
{
	switch( cp->len ) {
	case 4:
		g_snprintf(
			b,
			MAX_GUI_BUFFER,
			"%-29s= %02X%02X%02X%02X",
			n,
			cp->p_raw[ 0 ], cp->p_raw[ 1 ], cp->p_raw[ 2 ], cp->p_raw[ 3 ]
		);
		col_append_fstr(
			PINFO->cinfo,
			COL_INFO,
			" (ID: %02X%02X%02X%02X) ",
			cp->p_raw[ 0 ], cp->p_raw[ 1 ], cp->p_raw[ 2 ], cp->p_raw[ 3 ]
		);
		break;
	case 8:
		g_snprintf(
			b,
			MAX_GUI_BUFFER,
			"%-29s= Invoke:%02X%02X%02X%02X Responding:%02X%02X%02X%02X", 
			n,
			cp->p_raw[ 0 ], cp->p_raw[ 1 ], cp->p_raw[ 2 ], cp->p_raw[ 3 ],
			cp->p_raw[ 4 ], cp->p_raw[ 5 ], cp->p_raw[ 6 ], cp->p_raw[ 7 ]
		);
		col_append_fstr(
			PINFO->cinfo,
			COL_INFO,
			" (ID: %02X%02X%02X%02X, Responding ID: %02X%02X%02X%02X) ",
			cp->p_raw[ 0 ], cp->p_raw[ 1 ], cp->p_raw[ 2 ], cp->p_raw[ 3 ],
			cp->p_raw[ 4 ], cp->p_raw[ 5 ], cp->p_raw[ 6 ], cp->p_raw[ 7 ]
		);
		break;
	default:
		return( 0 );	// Failure
	}
	return( 1 );
}

static	gint	Compo_ID( char * n, cp_t * cp, char * b )
{
	switch( cp->len ) {
	case 1:
		g_snprintf(
			b,
			MAX_GUI_BUFFER,
			"%-29s= Invoke:%u",
			n,
			cp->p_raw[ 0 ]
		);
		break;
	case 2:
		g_snprintf(
			b,
			MAX_GUI_BUFFER,
			"%-29s= Invoke:%u Correlation:%u",
			n,
			cp->p_raw[ 0 ], cp->p_raw[ 1 ]
		);
		break;
	default:
		return( 0 );	// Failure
	}
	return( 1 );
}

/*
 *	Private Operations Tables (Per Family)
 */

static	char	*	PO64[ 32 ] = {
	"Request/Notification:",		// 0 is Family Name
	NULL,					// 1
	"collectedInformation",			// 2
	"analyzedInformation",			// 3
	NULL,					// 4
	"terminationAttempt",			// 5
	"tBusy",				// 6
	"oCalledPartyBusy",			// 7
	"tNoAnswer",				// 8
	"oNoAnswer",				// 9
	"tAnswer",				// 10
	"oAnswer",				// 11
	"oTermSeized",				// 12
	"facilitySelectedAndAvailable",		// 13
	NULL,					// 14
	NULL,					// 15
	NULL,					// 16
	NULL,					// 17
	NULL,					// 18
	NULL,					// 19
	"timeout",				// 20
	NULL,					// 21
	NULL,					// 22
	"routeSelectFailure",			// 23
	"originationAttempt",			// 24
	"oSuspended",				// 25
	"oDisconnect",				// 26
	"oDTMFEntered",				// 27
	NULL
};

static	char	*	PO65[ 32 ] = {
	"Connection Control:",			// 0 is Family Name
	"analyzeRoute",				// 1
	"authorizeTermination",			// 2
	"releaseCall",				// 3
	"offerCall",				// 4
	"collectInformation",			// 5
	NULL,					// 6
	NULL,					// 7
	NULL,					// 7
	NULL,					// 9
	NULL,					// 10
	NULL,					// 11
	NULL,					// 12
	"continue",				// 13
	NULL
};

static	char	*	PO66[ 32 ] = {
	"Caller Interaction:",			// 0 is Family Name
	"sendToResource",			// 1
	"resourceClear",			// 2
	"cancelResourceEvent",			// 3
	"callInfoFromResource",			// 4
	NULL
};

static	char	*	PO67[ 32 ] = {
	"Status Notification:",			// 0 is Family Name
	"monitorForChange",			// 1
	"statusReported",			// 2
	"monitorSuccess",			// 3
	"terminationNotification",		// 4
	"sendNotification",			// 5
	NULL
};

static	char	*	PO68[ 32 ] = {
	"Information Revision:",		// 0 is Family Name
	"updateRequest",			// 1
	NULL,					// 2
	"update",				// 3
	NULL
};

static	char	*	PO69[ 32 ] = {
	"Network Management:",			// 0 is Family Name
	"acg",					// 1
	NULL
};

static	char	*	PO6A[ 32 ] = {
	"Connectivity Control:",		// 0 is Family Name
	"forwardCall",				// 1
	NULL
};

static	char	*	PO6B[ 32 ] = {
	"NCA Signaling:",			// 0 is Family Name
	"nCARequest",				// 1
	"nCAData",				// 2
	NULL
};

static	char	*	PO6D[ 32 ] = {
	"Request Event:",			// 0 is Family Name
	"requestReportBCMEvent",		// 1
	NULL
};

static	char	*	PO6E[ 32 ] = {
	"Transaction Control:",			// 0 is Family Name
	"close",				// 1
	NULL
};

static	char	*	PO6F[ 32 ] = {
	"Abnormal:",				// 0 is Family Name
	"reportError",				// 1
	NULL
};

static	char	*	PO7F[ 32 ] = {
	"IN Phase 1 ITU Operations:",		// 0 is Family Name
	"activateServiceFiltering",		// 1
	"activityTest",				// 2
	"applyCharging",			// 3
	"applyChargingReport",			// 4
	"callInformationReport",		// 5
	"callInformationRequest",		// 6
	"eventNotificationCharging",		// 7
	"furnishChargingInformation",		// 8
	"assistRequestInstructions",		// 9
	"oMidCall",				// 10
	"requestNotificationChargingEvent",	// 11
	"resetTimer",				// 12
	"sendChargingInformation",		// 13
	"serviceFilteringResponse",		// 14
	"tMidCall",				// 15
	"instructionsToSRF",			// 16
	NULL
};

static	gint	Prvt_Op( char * n, cp_t * cp, char * b )
{
	gint	i;
	char **	po;

	if( cp->len != 2 ) return( 0 );

	n_matrix[ CONTEXT ][ PRIMATIVE ] = AIN_Context_Primative;
	n_matrix[ CONTEXT ][ CONSTRUCT ] = AIN_Context_Primative;
	if( check_col( PINFO->cinfo, COL_PROTOCOL ) ) {
		col_set_str( PINFO->cinfo, COL_PROTOCOL, "T1.667 A-IN" );
	}

	i = g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );

	po = NULL;
	switch( cp->p_raw[ 0 ] ) {
	case 0x64:	po = PO64;
			break;
	case 0x65:	po = PO65;
			break;
	case 0x66:	po = PO66;
			break;
	case 0x67:	po = PO67;
			break;
	case 0x68:	po = PO68;
			break;
	case 0x69:	po = PO69;
			break;
	case 0x6A:	po = PO6A;
			break;
	case 0x6B:	po = PO6B;
			break;
	case 0x6D:	po = PO6D;
			break;
	case 0x6E:	po = PO6E;
			break;
	case 0x6F:	po = PO6F;
			break;
	case 0x7F:	po = PO7F;
			break;
	}
	if( po != NULL ) {
		if( cp->p_raw[ 1 ] < 32 && po[ cp->p_raw[ 1 ] ] != NULL ) {
			i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "%s %s", po[ 0 ], po[ cp->p_raw[ 1 ] ] );
		} else {
			i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "%s %u", po[ 0 ], cp->p_raw[ 1 ] );
		}
	} else {
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Family 0x%02X: %u", cp->p_raw[ 0 ], cp->p_raw[ 1 ] );
	}
	return( 1 );
}

static	gint	Natl_Op( char * n, cp_t * cp, char * b )
{
	gint	i;

	if( cp->len != 2 ) return( 0 );

	n_matrix[ CONTEXT ][ PRIMATIVE ] = Context_Primative;
	n_matrix[ CONTEXT ][ CONSTRUCT ] = Context_Construct;

	if( check_col( PINFO->cinfo, COL_PROTOCOL ) ) {
		col_set_str( PINFO->cinfo, COL_PROTOCOL, "T1.114 TCAP" );
	}

	i = g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );
	switch( cp->p_raw[ 0 ] ) {
	case 0x81:
	case 0x01:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Parameter " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Provide Value" );
				break;
		case 0x02:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Set Value" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x82:
	case 0x02:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Charging " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Bill Call" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x83:
	case 0x03:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Provide Instructions " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Start" );
				break;
		case 0x02:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Assist" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x84:
	case 0x04:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Connection Control " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Connect" );
				break;
		case 0x02:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Temporary Connect" );
				break;
		case 0x03:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Disconnect" );
				break;
		case 0x04:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Forward Disconnect" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x85:
	case 0x05:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Caller Interaction " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Play Announcement" );
				break;
		case 0x02:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Play Announcement and Collect Digits" );
				break;
		case 0x03:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Indicate Information Waiting" );
				break;
		case 0x04:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Indicate Information Provided" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x86:
	case 0x06:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Send Notification " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "When Party Free" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x87:
	case 0x07:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Network Management " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Automatic Code Gap" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x88:
	case 0x08:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Procedural " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x00:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Report Assist Termination" );
				break;
		case 0x02:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Temporary Handover" );
				break;
		case 0x03:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Security" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x89:
	case 0x09:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Operation Control " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Cancel" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	case 0x8A:
	case 0x0A:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Report Event " );
		switch( cp->p_raw[ 1 ] ) {
		case 0x01:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Voice Message Available" );
				break;
		case 0x02:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Voice Message Retrieved" );
				break;
		default:	i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Specifier 0x%02X", cp->p_raw[ 1 ] );
				break;
		}
		break;
	default:
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, "Family 0x%02X, Specifier 0x%02X", cp->p_raw[ 0 ], cp->p_raw[ 1 ] );
		break;
	}
	if( ( cp->p_raw[ 0 ] & 0x80 ) != 0 ) {
		i += g_snprintf( &b[ i ], MAX_GUI_BUFFER - i, " - Reply Required" );
	}
	return( 1 );
}

static	gint	AMAslpID( char * n, cp_t * cp, char * b )
{
	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );
	BCD2Ascii( &cp->p_raw[ 0 ], &b, 0, 9 );
	return( 1 );
}

static	gint	Bcd( char * n, cp_t * cp, char * b )
{
	gint	count;

	count = ( cp->len << 1 );
	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );
	BCD2Ascii( &cp->p_raw[ 0 ], &b, 0, count );
	return( 1 );
}

static	gint	Bearer( char * n, cp_t * cp, char * b )
{
	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );
	switch( cp->p_raw[ 0 ] ) {
	case	0:	g_snprintf( b, MAX_GUI_BUFFER, "%s", "speech"			);
			break;
	case	1:	g_snprintf( b, MAX_GUI_BUFFER, "%s", "f31kHzAudio"		);
			break;
	case	2:	g_snprintf( b, MAX_GUI_BUFFER, "%s", "f7kHzAudio"		);
			break;
	case	3:	g_snprintf( b, MAX_GUI_BUFFER, "%s", "b56kbps"			);
			break;
	case	4:	g_snprintf( b, MAX_GUI_BUFFER, "%s", "b64kbps"			);
			break;
	case	5:	g_snprintf( b, MAX_GUI_BUFFER, "%s", "packetModeData"		);
			break;
	case	6:	g_snprintf( b, MAX_GUI_BUFFER, "%s", "multiRate"		);
			break;
	default:	g_snprintf( b, MAX_GUI_BUFFER, "unknown:%u", cp->p_raw[ 0 ]	);
			break;
	}
	return( 1 );
}

static	gint	Boolean( char * n, cp_t * cp, char * b )
{
	if( cp->len ) return( 0 );
	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s!", n );
	return( 1 );
}

static	gint	Carrier( char * n, cp_t * cp, char * b )
{
	static	char *	sel[ ] = {
		"no indication",
		"presubscribed & not input",
		"presubscribed & input",
		"presubscribed & no indication of input",
		"not presubscribed & input",
		NULL,
		NULL,
		NULL
	};
	guint8	slct;

	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );
	BCD2Ascii( &cp->p_raw[ 2 ], &b, 0, cp->p_raw[ 1 ] );
	slct = ( cp->p_raw[ 0 ] & 0x07 );
	if( slct != 0 ) b += g_snprintf( b, MAX_GUI_BUFFER, " - Selection:%u(%s)", slct, sel[ slct ] );
	return( 1 );
}

static	gint	Edp( char * n, cp_t * cp, char * b )
{
	static	char *	request[ ] = {

		"oCalledPartyBusy",		/* Bit  0 */
		"oNoAnswer",			/* Bit  1 */
		"oTermSeized",			/* Bit  2 */
		"oAnswer",			/* Bit  3 */
		"tBusy",			/* Bit  4 */
		"tNoAnswer",			/* Bit  5 */
		"termResourceAvailable",	/* Bit  6 */
		"tAnswer",			/* Bit  7 */

		"networkBusy",			/* Bit  8 */
		"oSuspended",			/* Bit  9 */
		"oDisconnectCalled",		/* Bit 10 */
		"oDisconnect",			/* Bit 11 */
		"oAbandon",			/* Bit 12 */
		"featureActivator",		/* Bit 13 */
		"switchHookFlash",		/* Bit 14 */
		"success",			/* Bit 15 */

		"tDisconnect",			/* Bit 16 */
		"timeout",			/* Bit 17 */
		"originationAttempt",		/* Bit 18 */
		"oDTMFEntered",			/* Bit 19 */
		"Bit20",			/* Bit 20 */
		"Bit21",			/* Bit 21 */
		"Bit22",			/* Bit 22 */
		"Bit23",			/* Bit 23 */

		NULL
	};
	gint	i;
	guint32	x;

	switch( cp->len ) {
	case 3:
		x = *((guint32 *)cp->p_raw);
		break;
	case 2:
		x = *((guint16 *)cp->p_raw);
		break;
	case 1:
		x = *((guint8 *)cp->p_raw);
		break;
	default:
		return( 0 );    // Failure
	}
	x &= 0x0FFFFF;
	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );
	for( i = 0; i < 24; i++ ) {
		if( ( x & ( 1 << i ) ) == 0 ) continue;
		b += g_snprintf( b, MAX_GUI_BUFFER, "%s+", request[ i ] );
	}
	b[ -1 ] = 0;
	return( 1 );
}

static	gint	EdpN( char * n, cp_t * cp, char * b )
{
	*((guint32 *)cp->p_raw) &= 0x0000CC;
	return( Edp( n, cp, b ) );
}

static	gint	EdpR( char * n, cp_t * cp, char * b )
{
	*((guint32 *)cp->p_raw) &= 0x0FFF33;
	return( Edp( n, cp, b ) );
}

static	gint	RedInf( char * n, cp_t * cp, char * b )
{
	static	char *	reason[ ] = { "unknown", "user-busy", "no-reply", "unconditional", NULL };

	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= Count:%u ", n, ( cp->p_raw[ 1 ] & 0x0F ) );
	b += g_snprintf( b, MAX_GUI_BUFFER, "Reason:%s ",  reason[ ( cp->p_raw[ 1 ] & 0xF0 ) >> 4 ] );
	b += g_snprintf( b, MAX_GUI_BUFFER, "Original:%s", reason[ ( cp->p_raw[ 0 ] & 0xF0 ) >> 4 ] );
	return( 1 );
}

static	gint	Number( char * n, cp_t * cp, char * b )
{
static	char	*	scr[ ] = {
				"Not Screened",
				"User Provided-Passed",
				"User Provided-Failed",
				"Network Provided"
			};
static	char	*	pln[ ] = {
				"Unknown",
				"ISDN-E.164",
				"reserved",
				"reserved",
				"reserved",
				"Private",
				"reserved",
				"reserved"
			};
static	char	*	prs[ ] = {
				"Allowed",
				"Restricted",
				"Number Unavailable",
				"reserved"
			};

	gint	count;
	guint8	non;
	guint8	plan;
	guint8	scrn;
	guint8	pres;

	count = ( ( cp->len - 2 ) << 1 );
	if( ( cp->p_raw[ 0 ] & 0x80 ) != 0 ) count -= 1;

	non  =   ( cp->p_raw[ 0 ] & 0x7F );
	plan = ( ( cp->p_raw[ 1 ] & 0x70 ) >> 4 );
	pres = ( ( cp->p_raw[ 1 ] & 0x0C ) >> 2 );
	scrn =   ( cp->p_raw[ 1 ] & 0x03 );

	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", n );
	if( count == 10 && ( non == 3 || non == 1 ) ) {
		BCD2Ascii( &cp->p_raw[ 2 ], &b, 0, 3 );
		*b++ = '-';
		BCD2Ascii( &cp->p_raw[ 2 ], &b, 3, 3 );
		*b++ = '-';
		BCD2Ascii( &cp->p_raw[ 2 ], &b, 6, 4 );
	} else if( count == 7 && ( non == 3 || non == 1 ) ) {
		BCD2Ascii( &cp->p_raw[ 2 ], &b, 0, 3 );
		*b++ = '-';
		BCD2Ascii( &cp->p_raw[ 2 ], &b, 3, 4 );
	} else {
		BCD2Ascii( &cp->p_raw[ 2 ], &b, 0, count );
	}
	if( non != 0 || plan != 0 || pres != 0 || scrn != 0 ) b += g_snprintf( b, MAX_GUI_BUFFER, " -" );
	if( non != 0 ) {
		switch( cp->id ) {
		case  1:	/* Access Code */
		case  8:	/* AMA Digits Dialed WC */
		case 15:	/* Called Party */
		case 22:	/* Collected Address Info */
		case 23:	/* Collected Digits */
		case 35:	/* LATA */
		case 37:	/* Outpulse Number */
		case 51:	/* Tcm */
		case 55:	/* Vertical Service Code */
		case 86:	/* Destination Address */
			switch( non ) {
			case	0:
				break;
			case	1:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Subscriber)", non );
				break;
			case	3:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(National)", non );
				break;
			case	4:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(International)", non );
				break;
			case	113:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Subscriber & Operator Requested)", non );
				break;
			case	114:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(National & Operator Requested)", non );
				break;
			case	115:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(International & Operator Requested)", non );
				break;
			case	116:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Operator Requested)", non );
				break;
			case	117:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Pass To Carrier)", non );
				break;
			case	118:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(950+ Call)", non );
				break;
			case	119:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Test Call)", non );
				break;
			default:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u", non );
				break;
			}
			break;
		case  7:	/* AMA Business Customer ID */
		case  9:	/* AMA Line Number */
		case 18:	/* Calling Party ID */
		case 36:	/* Original Called Party ID */
		case 43:	/* Redirecting Party ID */
			switch( non ) {
			case	0:
				break;
			case	1:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Subscriber)", non );
				break;
			case	3:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(National)", non );
				break;
			case	4:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(International)", non );
				break;
			case	113:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Non-unique Subscriber)", non );
				break;
			case	115:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Non-unique National)", non );
				break;
			case	116:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Non-unique International)", non );
				break;
			case	119:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Test Line)", non );
				break;
			default:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u", non );
				break;
			}
			break;
		case  6:	/* AMA Alternate Billing Number */
		case 19:	/* Charge Number */
			switch( non ) {
			case	0:
				break;
			case	1:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Calling Party ANI; Subscriber)", non );
				break;
			case	2:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Calling Party ANI Not Included)", non );
				break;
			case	3:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Calling Party ANI; National)", non );
				break;
			case	5:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Called Party ANI; Subscriber)", non );
				break;
			case	6:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Called Party ANI; Not Included)", non );
				break;
			case	7:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u(Called Party ANI; National)", non );
				break;
			default:
				b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u", non );
				break;
			}
			break;
		default:
			if( non != 0 ) b += g_snprintf( b, MAX_GUI_BUFFER, " Nature:%u", non );
			break;
		}
	}
	if( plan != 0 ) b += g_snprintf( b, MAX_GUI_BUFFER, " Plan:%u(%s)", plan, pln[ plan ] );
	if( pres != 0 ) b += g_snprintf( b, MAX_GUI_BUFFER, " Presentation:%u(%s)", pres, prs[ pres ] );
	if( scrn != 0 ) b += g_snprintf( b, MAX_GUI_BUFFER, " Screen:%u(%s)", scrn, scr[ scrn ] );
	return( 1 );
}

static	char	*	digits_type[ ] = {
		NULL,
		"Called Party Number",
		"Calling Party Number",
		"Caller Interaction",
		"Routing Number",
		"Billing Number",
		"Destination Number",
		"LATA",
		"Carrier",
		"Last Calling Party",
		"Last Party Called",
		"Calling Directory Number",
		"VMSR Identifier",
		"Original Called Number",
		"Redirecting Number",
		"Connected Number",
		NULL
};

static	gint	Digits( char * n, cp_t * cp, char * b )
{
	gint	i;
	char *	p;

	p = digits_type[ cp->p_raw[ 0 ] & 0x0F ];
	if( p == NULL ) p = n;
	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", p );
	for( i = 0; i < cp->p_raw[ 3 ]; i++ ) {
		*b++ = "0123456789ABCDEF"[ BCD_digit( &cp->p_raw[ 4 ], i ) ];
		if( cp->p_raw[ 3 ] == 10 && ( cp->p_raw[ 1 ] & 1 ) == 0 ) {
			switch( i ) {
			case 2:
			case 5:	*b++ = '-';
			}
			continue;
		}
		if( cp->p_raw[ 3 ] == 7 && ( cp->p_raw[ 1 ] & 1 ) == 0 ) {
			switch( i ) {
			case 2:	*b++ = '-';
			}
			continue;
		}
	}
	*b = 0;
	return( 1 );
}

static	char	*	name_type[ 8 ] = {
		NULL,
		"Calling Name",
		"Original Called Name",
		"Redirecting Name",
		"Connected Name",
		NULL
};

static	gint	G_Name( char * n, cp_t * cp, char * b )
{
	char	*	g;

	g = name_type[ ( ( cp->p_raw[ 0 ] >> 5 ) & 0x07 ) ];
	if( g == NULL || *g == 0 ) g = n;
	
	b += g_snprintf(
		b,
		MAX_GUI_BUFFER,
		"%-29s= \"%s\"",
		g,
		( (int)cp->len > (int)1 ? (char *)&cp->p_raw[ 1 ] : "" )
	);
	return( 1 );
}

static	gint	Natl_Err( char * n, cp_t * cp, char * b )
{
static	char	*	errors[ 32 ] = {
	NULL,
	"Unexpected Component Sequence",
	"Unexpected Data Value",
	"Unavailable Resource",
	"Missing Customer Record",
	NULL,
	"Data Unavailable",
	"Task Refused",
	"Queue Full",
	"No Queue",
	"Timer Expired",
	"Data Already Exists",
	"Unauthorized Request",
	"Not Queued",
	"Unassigned DN",
	NULL,
	"Notification Unavailable To Destination DN",
	"VMSR System Identification Did Not Match User Profile",
	"Security Error",
	"Missing Parameter",
	"Unexpected Parameter Sequence",
	"Unexpected Message",
	"Unexpected Package Type",
	NULL
};
	if( cp->len != 1 || cp->p_raw[ 0 ] >= 32 || errors[ cp->p_raw[ 0 ] ] == NULL ) return( 0 );

	b += g_snprintf(
		b,
		MAX_GUI_BUFFER,
		"%-29s= %s",
		n,
		errors[ cp->p_raw[ 0 ] ]
	);
	return( 1 );
}

static	gint	BillInd( char * n, cp_t * cp, char * b )
{
	if( cp->len != 4 || n == NULL ) return( 0 );

	b += g_snprintf( b, MAX_GUI_BUFFER, "%-29s= ", "AMA Call Type" );
	BCD2Ascii( &cp->p_raw[ 0 ], &b, 0, 3 );
	if( cp->p_raw[ 2 ] != 0 && ( cp->p_raw[ 3 ] & 0x0F ) != 0 ) {
		b += g_snprintf( b, MAX_GUI_BUFFER, ", ServiceFeature= " );
		BCD2Ascii( &cp->p_raw[ 2 ], &b, 0, 3 );
	}
	*b++ = 0;
	return( 1 );
}

static	gint	ResrcTyp( char * n, cp_t * cp, char * b )
{
static	char	*	rtypes[ ] = {
	"Play Anouncements",
	"Play Anouncements and Collect Digits",
	"Text-to-Speech",
	"Text-to-Speech and Collect Digits",
	"Flex Parameter Block",
	NULL
};
	guint8	rt;

	if( cp->len != 1 ) return( 0 );

	rt = cp->p_raw[ 0 ] & 0x7F;

	b += g_snprintf(
		b,
		MAX_GUI_BUFFER,
		"%-29s= %u (%s)",
		n,
		rt,
		( rt > 4 ? "reserved" : rtypes[ rt ] )
	);
	return( 1 );
}

static	gint	Trig_Typ( char * n, cp_t * cp, char * b )
{
static	char	*	trigger[ 64 ] = {
	"featureActivator",
	"verticalServiceCode",
	"customizedAccess",
	"customizedIntercom",
	"npa",
	"npaNXX",
	"nxx",
	"nxxXXXX",
	"npaNXXXXXX",
	"countryCodeNPANXXXXXX",
	"carrierAccess",
	"prefixes",
	"n11",
	"aFR",
	"sharedIOTrunk",
	"terminationAttempt",
	"offHookImmediate",
	"offHookDelay",
	"channelSetupPRI",
	"npaN",
	"npaNX",
	"npaNXXX",
	"npaNXXXX",
	"npaNXXXXX",
	"networkBusy",
	"tNoAnswer",
	"tBusy",
	"oCalledPartyBusy",
	"specificFeatureCode",
	"oNoAnswer",
	"priNetworkServices",
	"oSwitchHookFlashImmediate",
	"oFeatureActivator",
	"oSwitchHookFlashSpecifiedCode",
	"tSwitchHookFlashImmediate",
	"tFeatureActivator",
	"tSwitchHookFlashSpecifiedCode",
	"numberPortability",
	"onePlus",
	"specifiedCarrier",
	"international",
	"zeroPlus",
	"zeroMinus",
	NULL,
	NULL,
	NULL,
	"termResourceAvailable",
	NULL,
	NULL,
	"dedicatedTrunkGroup",
	"facilitySelectedAndAvailable",
	NULL
};
	guint8	t = cp->p_raw[ 0 ];

	if( cp->len != 1 ) return( 0 );

	b += g_snprintf(
		b,
		MAX_GUI_BUFFER,
		"%-29s= %u (%s)",
		n,
		t,
		( ( t >= 64 || trigger[ t ] == NULL ) ? "reserved" : trigger[ t ] )
	);
	return( 1 );
}

static	item_t	SONUS_Primative[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// [   0 ]
	{ NULL,					NULL	},	// [   1 ]
	{ NULL,					NULL	},	// [   2 ]
	{ NULL,					NULL	},	// [   3 ]
	{ NULL,					NULL	},	// [   4 ]
	{ NULL,					NULL	},	// [   5 ]
	{ NULL,					NULL	},	// [   6 ]
	{ NULL,					NULL	},	// [   7 ]
	{ NULL,					NULL	},	// [   8 ]
	{ NULL,					NULL	},	// [   9 ]
	{ NULL,					NULL	},	// [  10 ]
	{ NULL,					NULL	},	// [  11 ]
	{ NULL,					NULL	},	// [  12 ]
	{ NULL,					NULL	},	// [  13 ]
	{ NULL,					NULL	},	// [  14 ]
	{ NULL,					NULL	},	// [  15 ]
	{ NULL,					NULL	},	// [  16 ]
	{ NULL,					NULL	},	// [  17 ]
	{ NULL,					NULL	},	// [  18 ]
	{ NULL,					NULL	},	// [  19 ]
	{ NULL,					NULL	},	// [  20 ]
	{ NULL,					NULL	},	// [  21 ]
	{ NULL,					NULL	},	// [  22 ]
	{ NULL,					NULL	},	// [  23 ]
	{ NULL,					NULL	},	// [  24 ]
	{ NULL,					NULL	},	// [  25 ]
	{ NULL,					NULL	},	// [  26 ]
	{ NULL,					NULL	},	// [  27 ]
	{ NULL,					NULL	},	// [  28 ]
	{ NULL,					NULL	},	// [  29 ]
	{ NULL,					NULL	},	// [  30 ]
	{ NULL,					NULL	},	// [  31 ]
	{ NULL,					NULL	},	// [  32 ]
	{ NULL,					NULL	},	// [  33 ]
	{ NULL,					NULL	},	// [  34 ]
	{ NULL,					NULL	},	// [  35 ]
	{ NULL,					NULL	},	// [  36 ]
	{ NULL,					NULL	},	// [  37 ]
	{ NULL,					NULL	},	// [  38 ]
	{ NULL,					NULL	},	// [  39 ]
	{ NULL,					NULL	},	// [  40 ]
	{ NULL,					NULL	},	// [  41 ]
	{ NULL,					NULL	},	// [  42 ]
	{ NULL,					NULL	},	// [  43 ]
	{ NULL,					NULL	},	// [  44 ]
	{ NULL,					NULL	},	// [  45 ]
	{ NULL,					NULL	},	// [  46 ]
	{ NULL,					NULL	},	// [  47 ]
	{ NULL,					NULL	},	// [  48 ]
	{ NULL,					NULL	},	// [  49 ]
	{ NULL,					NULL	},	// [  50 ]
	{ NULL,					NULL	},	// [  51 ]
	{ NULL,					NULL	},	// [  52 ]
	{ NULL,					NULL	},	// [  53 ]
	{ NULL,					NULL	},	// [  54 ]
	{ NULL,					NULL	},	// [  55 ]
	{ NULL,					NULL	},	// [  56 ]
	{ NULL,					NULL	},	// [  57 ]
	{ NULL,					NULL	},	// [  58 ]
	{ NULL,					NULL	},	// [  59 ]
	{ NULL,					NULL	},	// [  60 ]
	{ NULL,					NULL	},	// [  61 ]
	{ NULL,					NULL	},	// [  62 ]
	{ NULL,					NULL	},	// [  63 ]
	{ NULL,					NULL	},	// [  64 ]
	{ NULL,					NULL	},	// [  65 ]
	{ NULL,					NULL	},	// [  66 ]
	{ NULL,					NULL	},	// [  67 ]
	{ NULL,					NULL	},	// [  68 ]
	{ NULL,					NULL	},	// [  69 ]
	{ NULL,					NULL	},	// [  70 ]
	{ NULL,					NULL	},	// [  71 ]
	{ NULL,					NULL	},	// [  72 ]
	{ NULL,					NULL	},	// [  73 ]
	{ NULL,					NULL	},	// [  74 ]
	{ NULL,					NULL	},	// [  75 ]
	{ NULL,					NULL	},	// [  76 ]
	{ NULL,					NULL	},	// [  77 ]
	{ NULL,					NULL	},	// [  78 ]
	{ NULL,					NULL	},	// [  79 ]
	{ NULL,					NULL	},	// [  80 ]
	{ NULL,					NULL	},	// [  81 ]
	{ NULL,					NULL	},	// [  82 ]
	{ NULL,					NULL	},	// [  83 ]
	{ NULL,					NULL	},	// [  84 ]
	{ NULL,					NULL	},	// [  85 ]
	{ NULL,					NULL	},	// [  86 ]
	{ NULL,					NULL	},	// [  87 ]
	{ NULL,					NULL	},	// [  88 ]
	{ NULL,					NULL	},	// [  89 ]
	{ NULL,					NULL	},	// [  90 ]
	{ NULL,					NULL	},	// [  91 ]
	{ NULL,					NULL	},	// [  92 ]
	{ NULL,					NULL	},	// [  93 ]
	{ NULL,					NULL	},	// [  94 ]
	{ NULL,					NULL	},	// [  95 ]
	{ NULL,					NULL	},	// [  96 ]
	{ NULL,					NULL	},	// [  97 ]
	{ NULL,					NULL	},	// [  98 ]
	{ NULL,					NULL	},	// [  99 ]
	{ "Network Location ID",		Bcd	},	// [ 100 ]
	{ "SCP Call ID",			Decimal	},	// [ 101 ]
	{ "Forward AIN Indicator",		NULL	},	// [ 102 ]
	{ "Backward AIN Indicator",		NULL	},	// [ 103 ]
	{ "Business Service Group",		Decimal	},	// [ 104 ]
	{ "Route Plan Indicator",		Bcd	},	// [ 105 ]
	{ "Overflow Route Plan Indicator",	Bcd	},	// [ 106 ]
	{ "Connected Announcement ID",		Decimal	},	// [ 107 ]
	{ "SCP Treatment",			Decimal	},	// [ 108 ]
	{ NULL,					NULL	}
};

static	gint	Direct( char * n, cp_t * cp, char * b )
{
	if( cp->len        == 6    &&
	    cp->p_raw[ 0 ] == 0x2A &&
	    cp->p_raw[ 1 ] == 0x86 &&
	    cp->p_raw[ 2 ] == 0x48 &&
	    cp->p_raw[ 3 ] == 0x86 &&
	    cp->p_raw[ 4 ] == 0xFD &&
	    cp->p_raw[ 5 ] == 0x1C ) {
		b += g_snprintf(
			b,
			MAX_GUI_BUFFER,
			"%-29s= SONUS Extensions",
			n
		);
		n_matrix[ CONTEXT ][ PRIMATIVE ] = SONUS_Primative;
		return( 1 );
	}
	return( 0 );
}

static	item_t	Universal_Construct[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// 0
	{ NULL,					NULL	},	// 1
	{ NULL,					NULL	},	// 2
	{ NULL,					NULL	},	// 3
	{ NULL,					NULL	},	// 4
	{ NULL,					NULL	},	// 5
	{ NULL,					NULL	},	// 6
	{ NULL,					NULL	},	// 7
	{ "External Identifier",		NULL	},	// 8
	{ NULL,					NULL	},	// 9
	{ NULL,					NULL	},	// 10
	{ NULL,					NULL	},	// 11
	{ NULL,					NULL	},	// 12
	{ NULL,					NULL	},	// 13
	{ NULL,					NULL	},	// 14
	{ NULL,					NULL	},	// 15
	{ "Prameter Sequence",			NULL	},	// 16
	{ NULL,					NULL	},	// 17
	{ NULL,					NULL	},	// 18
	{ NULL,					NULL	},	// 19
	{ NULL,					NULL	}
};

static	item_t	Universal_Primative[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// 0
	{ NULL,					NULL	},	// 1
	{ "Indirect Reference ID",		NULL	},	// 2
	{ NULL,					NULL	},	// 3
	{ "Octet String",			NULL	},	// 4
	{ NULL,					NULL	},	// 5
	{ "Direct Reference ID",		Direct	},	// 6
	{ "Data Value",				NULL	},	// 7
	{ NULL,					NULL	},	// 8
	{ NULL,					NULL	},	// 9
	{ NULL,					NULL	},	// 10
	{ NULL,					NULL	},	// 11
	{ NULL,					NULL	},	// 12
	{ NULL,					NULL	},	// 13
	{ NULL,					NULL	},	// 14
	{ NULL,					NULL	},	// 15
	{ NULL,					NULL	},	// 16
	{ NULL,					NULL	},	// 17
	{ NULL,					NULL	},	// 18
	{ NULL,					NULL	},	// 19
	{ NULL,					NULL	},	// 20
	{ NULL,					NULL	},	// 21
	{ NULL,					NULL	},	// 22
	{ "Timestamp",				NULL	},	// 23
	{ NULL,					NULL	},	// 24
	{ NULL,					NULL	},	// 25
	{ NULL,					NULL	},	// 26
	{ NULL,					NULL	},	// 27
	{ NULL,					NULL	},	// 28
	{ NULL,					NULL	},	// 29
	{ NULL,					NULL	}
};

static	item_t	National_Construct[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// 0
	{ "Unidirectional",			NULL	},	// 1
	{ "Query With Permission",		NULL	},	// 2
	{ "Query Without Permission",		NULL	},	// 3
	{ "Response",				NULL	},	// 4
	{ "Conversation With Permission",	NULL	},	// 5
	{ "Conversation Without Permission",	NULL	},	// 6
	{ NULL,					NULL	},	// 7
	{ "Component Sequence",			NULL	},	// 8
	{ "Invoke (Last)",			NULL	},	// 9
	{ "Return Result (Last)",		NULL	},	// 10
	{ "Return Error",			NULL	},	// 11
	{ "Reject",				NULL	},	// 12
	{ "Invoke (Not Last)",			NULL	},	// 13
	{ "Return Result (Not Last)",		NULL	},	// 14
	{ NULL,					NULL	},	// 15
	{ NULL,					NULL	},	// 16
	{ NULL,					NULL	},	// 17
	{ "Parameter Set",			NULL	},	// 18
	{ NULL,					NULL	},	// 19
	{ NULL,					NULL	},	// 20
	{ NULL,					NULL	},	// 21
	{ "Abort",				NULL	},	// 22
	{ "User Abort Information",		NULL	},	// 24
	{ "Dialogue Portion",			NULL	},	// 25
	{ NULL,					NULL	},	// 26
	{ NULL,					NULL	},	// 27
	{ NULL,					NULL	},	// 28
	{ "User Information",			NULL	},	// 29
	{ NULL,					NULL	}
};

static	item_t	National_Primative[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// 0
	{ NULL,					NULL	},	// 1
	{ NULL,					NULL	},	// 2
	{ NULL,					NULL	},	// 3
	{ NULL,					NULL	},	// 4
	{ NULL,					NULL	},	// 5
	{ NULL,					NULL	},	// 6
	{ "Transaction ID",			Tran_ID	},	// 7
	{ NULL,					NULL	},	// 8
	{ NULL,					NULL	},	// 9
	{ NULL,					NULL	},	// 10
	{ NULL,					NULL	},	// 11
	{ NULL,					NULL	},	// 12
	{ NULL,					NULL	},	// 13
	{ NULL,					NULL	},	// 14
	{ "Component ID",			Compo_ID},	// 15
	{ "National TCAP Operation",		Natl_Op	},	// 16
	{ "Private TCAP Operation",		Prvt_Op	},	// 17
	{ NULL,					NULL	},	// 18
	{ "National TCAP Error Code",		Natl_Err},	// 19
	{ "Private TCAP Error Code",		NULL	},	// 20
	{ "Problem Code",			NULL	},	// 21
	{ NULL,					NULL	},	// 22
	{ "P-Abort Cause",			NULL	},	// 23
	{ "User Abort Information",		NULL	},	// 24
	{ NULL,					NULL	},	// 25
	{ "Protocol Version",			NULL	},	// 26
	{ "Application Context ID",		NULL	},	// 27
	{ "Object Identifier",			NULL	},	// 28
	{ NULL,					NULL	},	// 29
	{ NULL,					NULL	},	// 30
	{ NULL,					NULL	},	// 31
	{ NULL,					NULL	},	// 32
	{ NULL,					NULL	},	// 33
	{ NULL,					NULL	},	// 34
	{ NULL,					NULL	},	// 35
	{ NULL,					NULL	},	// 36
	{ NULL,					NULL	},	// 37
	{ NULL,					NULL	},	// 38
	{ NULL,					NULL	},	// 39
	{ NULL,					NULL	},	// 40
	{ NULL,					NULL	},	// 41
	{ NULL,					NULL	},	// 42
	{ NULL,					NULL	},	// 43
	{ NULL,					NULL	},	// 44
	{ NULL,					NULL	},	// 45
	{ NULL,					NULL	},	// 46
	{ NULL,					NULL	},	// 47
	{ NULL,					NULL	},	// 48
	{ NULL,					NULL	},	// 49
	{ NULL,					NULL	},	// 50
	{ NULL,					NULL	},	// 51
	{ NULL,					NULL	},	// 52
	{ NULL,					NULL	},	// 53
	{ NULL,					NULL	},	// 54
	{ NULL,					NULL	},	// 55
	{ NULL,					NULL	},	// 56
	{ NULL,					NULL	},	// 57
	{ NULL,					NULL	},	// 58
	{ NULL,					NULL	},	// 59
	{ NULL,					NULL	},	// 60
	{ NULL,					NULL	},	// 61
	{ NULL,					NULL	},	// 62
	{ NULL,					NULL	},	// 63
	{ NULL,					NULL	},	// 64
	{ "Billing Indicators",			BillInd	},	// 65 GR-1428
	{ "Connect Time",			NULL	},	// 66 GR-1428
	{ "Echo Data",				NULL	},	// 67 GR-1428
	{ NULL,					NULL	},	// 68
	{ "Originating Station Type",		Decimal	},	// 69 GR-1428
	{ "Termination Indicators",		NULL	},	// 70 GR-1428
	{ "ACG Indicators",			NULL	},	// 71 GR-1428
	{ "CIC Expansion Indicator",		Decimal	},	// 72
	{ NULL,					NULL	},	// 73
	{ NULL,					NULL	},	// 74
	{ NULL,					NULL	},	// 75
	{ NULL,					NULL	},	// 76
	{ NULL,					NULL	},	// 77
	{ NULL,					NULL	},	// 78
	{ NULL,					NULL	},	// 79
	{ NULL,					NULL	}
};

static	item_t	Context_Construct[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// 0
	{ NULL,					NULL	},	// 1
	{ NULL,					NULL	},	// 2
	{ NULL,					NULL	},	// 3
	{ NULL,					NULL	},	// 4
	{ NULL,					NULL	},	// 5
	{ NULL,					NULL	},	// 6
	{ NULL,					NULL	},	// 7
	{ NULL,					NULL	},	// 8
	{ NULL,					NULL	},	// 9
	{ "Service Key",			NULL	},	// 10
	{ NULL,					NULL	},	// 11
	{ NULL,					NULL	}
};

static	item_t	Context_Primative[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// 0
	{ "Automatic Code Gap Indicators",	NULL	},	// 1
	{ "Standard Announcment",		NULL	},	// 2
	{ "Customized Announcment",		NULL	},	// 3
	{ "Digits",				Digits	},	// 4
	{ "Standard User Error Code",		NULL	},	// 5
	{ "Problem Data",			NULL	},	// 6
	{ "SCCP Calling Party Address",		NULL	},	// 7
	{ "TransAction ID",			NULL	},	// 8
	{ "Package Type",			NULL	},	// 9
	{ "Service Key",			NULL	},	// 10
	{ "Busy/Idle Status",			NULL	},	// 11
	{ "Call Forwarding Status",		NULL	},	// 12
	{ "Originating Restrictions",		NULL	},	// 13
	{ "Terminating Restrictions",		NULL	},	// 14
	{ "Directory Num To Line Service Mapping", NULL	},	// 15
	{ "Duration",				NULL	},	// 16
	{ "Returned Data",			NULL	},	// 17
	{ "Bearer Capability Requested",	NULL	},	// 18
	{ "Bearer Capability Supported",	NULL	},	// 19
	{ "Reference ID",			NULL	},	// 20
	{ "Business Group",			NULL	},	// 21
	{ "Signalling Networks Identifier",	NULL	},	// 22
	{ "Generic Name",			G_Name	},	// 23
	{ "Message Waiting Indicator Type",	NULL	},	// 24
	{ "Look Ahead For Busy",		NULL	},	// 25
	{ "Circuit Identification Code",	NULL	},	// 26
	{ "Precedence Identifier",		NULL	},	// 27
	{ "Call Reference Identifier",		NULL	},	// 28
	{ "Authorization",			NULL	},	// 29
	{ "Integrity",				NULL	},	// 30
	{ "Sequence Number",			NULL	},	// 31
	{ "Number Of Messages",			NULL	},	// 32
	{ "Display Text",			NULL	},	// 33
	{ "Key Exchange",			NULL	},	// 34
	{ "SCCP Called Party Address",		NULL	},	// 35
	{ NULL,					NULL	}
};

static	item_t	Announcement_Context[ MAX_ELEMENTS ] = {
	{ "Maximum Digits",			Decimal	},	// [   0 ]
	{ "Uninteruptable Announcement",	NULL	},	// [   1 ]
	{ "Interuptable Announcement",		NULL	},	// [   2 ]
	{ NULL,					NULL	}
};

static	gint	Announc( char * n, cp_t * cp, char * b )
{
	if( n != NULL && b != NULL && cp->construct != 0 ) {
		n_matrix[ CONTEXT ][ CONSTRUCT ] = Announcement_Context;
		n_matrix[ CONTEXT ][ PRIMATIVE ] = Announcement_Context;
	}
	return( 0 );
}

static	item_t	Str_Parm_Context_Construct[ MAX_ELEMENTS ] = {
	{ "Announcement Block",			Announc	},	// [   0 ]
	{ "Announcement Digit Block",		Announc	},	// [   1 ]
	{ "Flex Parameter Block",		NULL	},	// [   2 ]
	{ "Correlation Block",			NULL	},	// [   3 ]
	{ NULL,					NULL	}
};

static	gint	StrParm( char * n, cp_t * cp, char * b )
{
	if( n != NULL && b != NULL && cp->construct != 0 ) {
		n_matrix[ CONTEXT ][ CONSTRUCT ] = Str_Parm_Context_Construct;
	}
	return( 0 );
}

static	item_t	User_ID_Context_Primative[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// [   0 ]
	{ "Directory Number",			Bcd	},	// [   1 ]
	{ "SPID & Directory Number",		Bcd	},	// [   2 ]
	{ NULL,					NULL	},	// [   3 ]
	{ NULL,					NULL	},	// [   4 ]
	{ "Trunk Group ID",			Decimal	},	// [   5 ]
	{ "Private Facility ID",		Decimal	},	// [   6 ]
	{ NULL,					NULL	}
};

static	gint	User_ID( char * n, cp_t * cp, char * b )
{
	if( n != NULL && b != NULL && cp->construct != 0 ) {
		n_matrix[ CONTEXT ][ PRIMATIVE ] = User_ID_Context_Primative;
	}
	return( 0 );
}

#ifdef	JUNK

static	item_t	AIN_Context_Construct[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// 0
	{ NULL,					NULL	},	// 1
	{ NULL,					NULL	},	// 2
	{ NULL,					NULL	},	// 3
	{ NULL,					NULL	},	// 4
	{ NULL,					NULL	},	// 5
	{ NULL,					NULL	},	// 6
	{ NULL,					NULL	},	// 7
	{ NULL,					NULL	},	// 8
	{ NULL,					NULL	},	// 9
	{ NULL,					NULL	},	// 10
	{ NULL,					NULL	},	// 11
	{ NULL,					NULL	}
};

#endif

static	item_t	AIN_Context_Primative[ MAX_ELEMENTS ] = {
	{ NULL,					NULL	},	// [   0 ]
	{ "Access Code",			Number	},	// [   1 ]
	{ "ACG Encountered",			NULL	},	// [   2 ]
	{ "Alternate Billing Indicator",	NULL	},	// [   3 ]
	{ "Alternate Carrier",			Carrier	},	// [   4 ]
	{ "Alternate Trunk Group",		Decimal	},	// [   5 ]
	{ "AMA Alternate Billing Number",	Number	},	// [   6 ]
	{ "AMA Business Customer ID",		Number	},	// [   7 ]
	{ "AMA Digits Dialed WC",		Number	},	// [   8 ]
	{ "AMA Line Number",			Number	},	// [   9 ]
	{ "AMA slp ID",				AMAslpID},	// [  10 ]
	{ "Amp1",				NULL	},	// [  11 ]
	{ "Answer Indicator",			Boolean	},	// [  12 ]
	{ "Bearer Capability",			Bearer	},	// [  13 ]
	{ "Busy Cause",				NULL	},	// [  14 ]
	{ "Called Party",			Number	},	// [  15 ]
	{ "Called Party Station Type",		Decimal	},	// [  16 ]
	{ "Calling Party BG",			NULL	},	// [  17 ]
	{ "Calling Party",			Number	},	// [  18 ]
	{ "Charge Number",			Number	},	// [  19 ]
	{ "Charge Party Station Type",		Decimal	},	// [  20 ]
	{ "Clear Cause",			Decimal	},	// [  21 ]
	{ "Collected Address Info",		Number	},	// [  22 ]
	{ "Collected Digits",			Number	},	// [  23 ]
	{ "Controlling Leg Treatment",		NULL	},	// [  24 ]
	{ "Disconnect Flag",			Boolean	},	// [  25 ]
	{ "Display Text",			NULL	},	// [  26 ]
	{ "Trunk Group ID",			NULL	},	// [  27 ]
	{ "Private Facility ID",		NULL	},	// [  28 ]
	{ "Mlhg",				NULL	},	// [  29 ]
	{ "Route Index",			NULL	},	// [  30 ]
	{ "Facility Member ID",			NULL	},	// [  31 ]
	{ "Failure Cause",			NULL	},	// [  32 ]
	{ "Generic Name",			NULL	},	// [  33 ]
	{ NULL,					NULL	},	// [  34 ]
	{ "LATA",				Number	},	// [  35 ]
	{ "Original Called Party",		Number	},	// [  36 ]
	{ "Outpulse Number",			Number	},	// [  37 ]
	{ "Overflow Billing Indicator",		NULL	},	// [  38 ]
	{ "Passive Leg Treatment",		NULL	},	// [  39 ]
	{ "Primary Billing Indicator",		NULL	},	// [  40 ]
	{ "Carrier",				Carrier	},	// [  41 ]
	{ "Primary Trunk Group",		Decimal	},	// [  42 ]
	{ "Redirecting Party",			Number	},	// [  43 ]
	{ "Redirection Information",		RedInf	},	// [  44 ]
	{ "Resource Type",			ResrcTyp},	// [  45 ]
	{ "Second Alternate BillingIndicator",	NULL	},	// [  46 ]
	{ "Second Alternate Carrier",		Carrier	},	// [  47 ]
	{ "Second Alternate TrunkGroup",	Decimal	},	// [  48 ]
	{ "Spid",				NULL	},	// [  49 ]
	{ "Str Parameter Block",		StrParm	},	// [  50 ]
	{ "Tcm",				Number	},	// [  51 ]
	{ "Trigger Criteria Type",		Trig_Typ},	// [  52 ]
	{ "User ID",				User_ID	},	// [  53 ]
	{ "Vertical Service Code",		Number	},	// [  54 ]
	{ "Application Error String",		NULL	},	// [  55 ]
	{ "Error Cause",			NULL	},	// [  56 ]
	{ "Failed Message",			NULL	},	// [  57 ]
	{ "Connect Time",			NULL	},	// [  58 ]
	{ "Control Cause Indicator",		NULL	},	// [  59 ]
	{ "Echo Data",				NULL	},	// [  60 ]
	{ "Facility Status",			NULL	},	// [  61 ]
	{ "Gap Duration",			NULL	},	// [  62 ]
	{ "National Gap Interval",		NULL	},	// [  63 ]
	{ "Private Gap Interval",		NULL	},	// [  64 ]
	{ "Monitor Time",			NULL	},	// [  65 ]
	{ "Status Cause",			NULL	},	// [  66 ]
	{ "Termination Indicator",		NULL	},	// [  67 ]
	{ "Trigger Criteria Flag",		NULL	},	// [  68 ]
	{ "Global Title Address",		NULL	},	// [  69 ]
	{ "Translation Type",			Decimal	},	// [  70 ]
	{ "AMA Measure",			NULL	},	// [  71 ]
	{ "Close Cause",			NULL	},	// [  72 ]
	{ "AMA Measurement",			NULL	},	// [  73 ]
	{ "Clear Cause Data",			NULL	},	// [  74 ]
	{ "Envelope Content",			NULL	},	// [  75 ]
	{ "DP Converter",			NULL	},	// [  76 ]
	{ NULL,					NULL	},	// [  77 ]
	{ "IP Return Block",			NULL	},	// [  78 ]
	{ "Carrier Usage",			NULL	},	// [  79 ]
	{ "Generic Address",			NULL	},	// [  80 ]
	{ "Sap",				NULL	},	// [  81 ]
	{ NULL,					NULL	},	// [  82 ]
	{ "Service Context",			NULL	},	// [  83 ]
	{ "Extension Parameter",		NULL	},	// [  84 ]
	{ "Security Envelope",			NULL	},	// [  85 ]
	{ "Destination Address",		Number	},	// [  86 ]
	{ "Ocn",				NULL	},	// [  87 ]
	{ NULL,					NULL	},	// [  88 ]
	{ "AMA Sequence Number",		NULL	},	// [  89 ]
	{ NULL,					NULL	},	// [  90 ]
	{ "O No Answer Timer",			Decimal	},	// [  91 ]
	{ "EDP Request",			EdpR	},	// [  92 ]
	{ "EDP Notification",			EdpN	},	// [  93 ]
	{ "Busy Type",				NULL	},	// [  94 ]
	{ "AMA BAF Modules",			NULL	},	// [  95 ]
	{ "STR Connection",			NULL	},	// [  96 ]
	{ NULL,					NULL	},	// [  97 ]
	{ "Envelope Encoding Authority",	NULL	},	// [  98 ]
	{ "T No Answer Timer",			Decimal	},	// [  99 ]
	{ "MWI",				NULL	},	// [ 100 ]
	{ "AMA Service Provider ID",		NULL	},	// [ 101 ]
	{ "Trigger Item Assignment",		NULL	},	// [ 102 ]
	{ "SSP User Resource",			NULL	},	// [ 103 ]
	{ NULL,					NULL	},	// [ 104 ]
	{ NULL,					NULL	},	// [ 105 ]
	{ "MsrID",				Number	},	// [ 106 ]
	{ "Generic Address List",		NULL	},	// [ 107 ]
	{ "Network Specific Facilities",	NULL	},	// [ 108 ]
	{ "Amp2",				NULL	},	// [ 109 ]
	{ NULL,					NULL	},	// [ 110 ]
	{ "Notification Indicator",		Boolean	},	// [ 111 ]
	{ "Failure Cause Data",			NULL	},	// [ 112 ]
	{ "Forward Call Indicator",		NULL	},	// [ 113 ]
	{ NULL,					NULL	},	// [ 114 ]
	{ "Alternate Dialing Plan Ind",		Number	},	// [ 115 ]
	{ "Disconnect Cause",			NULL	},	// [ 116 ]
	{ NULL,					NULL	},	// [ 117 ]
	{ NULL,					NULL	},	// [ 118 ]
	{ NULL,					NULL	},	// [ 119 ]
	{ NULL,					NULL	},	// [ 120 ]
	{ NULL,					NULL	},	// [ 121 ]
	{ NULL,					NULL	},	// [ 122 ]
	{ NULL,					NULL	},	// [ 123 ]
	{ NULL,					NULL	},	// [ 124 ]
	{ NULL,					NULL	},	// [ 125 ]
	{ NULL,					NULL	},	// [ 126 ]
	{ NULL,					NULL	},	// [ 127 ]
	{ NULL,					NULL	},	// [ 128 ]
	{ NULL,					NULL	},	// [ 129 ]
	{ NULL,					NULL	},	// [ 130 ]
	{ NULL,					NULL	},	// [ 131 ]
	{ NULL,					NULL	},	// [ 132 ]
	{ NULL,					NULL	},	// [ 133 ]
	{ NULL,					NULL	},	// [ 134 ]
	{ NULL,					NULL	},	// [ 135 ]
	{ NULL,					NULL	},	// [ 136 ]
	{ NULL,					NULL	},	// [ 137 ]
	{ NULL,					NULL	},	// [ 138 ]
	{ "Timeout Timer",			NULL	},	// [ 139 ]
	{ NULL,					NULL	},	// [ 140 ]
	{ NULL,					NULL	},	// [ 141 ]
	{ NULL,					NULL	},	// [ 142 ]
	{ NULL,					NULL	},	// [ 143 ]
	{ NULL,					NULL	},	// [ 144 ]
	{ NULL,					NULL	},	// [ 145 ]
	{ "Extended Ringing",			Boolean	},	// [ 146 ]
	{ NULL,					NULL	},	// [ 147 ]
	{ NULL,					NULL	},	// [ 148 ]
	{ "Generic Digits",			NULL	},	// [ 149 ]
	{ "Generic Digits List",		NULL	},	// [ 150 ]
	{ NULL,					NULL	},	// [ 151 ]
	{ NULL,					NULL	},	// [ 152 ]
	{ "DTMF Digits Detected",		NULL	},	// [ 153 ]
	{ "O DTMF Digits String",		NULL	},	// [ 154 ]
	{ "O DTMF Number Of Digits",		Decimal	},	// [ 155 ]
	{ "TSTRC Timer",			Decimal	},	// [ 156 ]
	{ NULL,					NULL	},	// [ 157 ]
	{ NULL,					NULL	},	// [ 158 ]
	{ NULL,					NULL	},	// [ 159 ]
	{ NULL,					NULL	},	// [ 160 ]
	{ NULL,					NULL	},	// [ 161 ]
	{ "CallingGeodeticLocation",		NULL	},	// [ 162 ]
	{ NULL,					NULL	}
};

static	item_t	*	n_matrix[ MAX_CLASS ][ MAX_TYPE ] = {
	{ Universal_Primative,	Universal_Construct	},	/* Universal	*/
	{ NULL,			NULL			},	/* Application	*/
	{ NULL,			NULL			},	/* Context	*/
	{ National_Primative,	National_Construct	},	/* Private	*/
	{ National_Primative,	National_Construct	}	/* National	*/
};

static	hf_register_info	hf[ ] = {
	{ &hf_seq,	{ "Byte Sequence",	"t1_114.seq",	FT_BYTES,	BASE_NONE,	NULL,	0,	"",	HFILL } },
};

static	gint		*	ett[ ] = {
	&ett_t1_114
};

static	void	print_line( cp_t * cp )
{
	guint32	i;
	guint32	len;
	gint	j;
	char *	n;
	char	name[ MAX_GUI_BUFFER ];
	char	buf[ MAX_GUI_BUFFER * 2 ];

	if( cp->construct ) {
		len = cp->id_len + cp->len_len;
	} else {
		len = cp->id_len + cp->len_len + cp->len;
	}

	if( global_t1_114_names &&
	    n_matrix[ cp->class ][ cp->construct ] != NULL &&
	    ( n = n_matrix[ cp->class ][ cp->construct ][ cp->id ].name ) != NULL ) {
		if( n_matrix[ cp->class ][ cp->construct ][ cp->id ].func != NULL ) {
			if( n_matrix[ cp->class ][ cp->construct ][ cp->id ].func(
				n_matrix[ cp->class ][ cp->construct ][ cp->id ].name,
				cp,
				name
			) ) {
				proto_tree_add_protocol_format(
					t1_114_tree,
					proto_t1_114,
					TVB,
					cp->offset,
					len,
					"%u %*.*s%s",
                                        depth,
                                        depth << 1,
                                        depth << 1,
                                        ". . . . . . . . . . . . . . . . . . . . ",
                                        name
				);
				return;
			}
		}
                if( depth == 0 ) col_set_str( PINFO->cinfo, COL_INFO, n );
                g_snprintf(
			name,
			sizeof( name ),
			"%s%c",
			n,
			( cp->construct ? ':' : '=' )
		);
	} else {
            g_snprintf(
			name,
			sizeof( name ),
			"%s %s %u%c",
			mclass[ cp->class ],
			mtype[ cp->construct ],
			cp->id,
			( cp->construct ? ':' : '=' )
	    );
            if( depth == 0 ) col_set_str( PINFO->cinfo, COL_INFO, name );
        }
	j = g_snprintf(
		buf,
		sizeof( buf ),
                "%u %*.*s%-30s Length=%-4u",
                depth,
                depth << 1,
                depth << 1,
                ". . . . . . . . . . . . . . . . . . . . ",
		name,
		cp->len
	);
	if( cp->construct == 0 ) {
		for( i = 0; i < cp->len; i += 1 ) {
			j += g_snprintf(
				&buf[ j ],
				( sizeof( buf ) - j ),
				" %02X",
				cp->p_raw[ i ]
			);
		}
	}
	proto_tree_add_protocol_format(
	        t1_114_tree,
		proto_t1_114,
		TVB,
		cp->offset,
		(cp->construct == 0 ? len : len + cp->len ),
		"%s",
		buf
	);
}

static	gint	get_octet( gint32 * decrement )
{
	gint	i;

	if( ( remainder -= 1 ) < 0 ) return( -1 );         // No data bytes remain - Malformed !!!
	if( decrement != NULL ) *decrement -= 1;
	if( bytes == NULL ) return( -2 );

        i = offset;
	offset += 1;
	return( bytes[ i ] );
}

static	gint32	get_length( gint32 * decrement, cp_t * cp )
{
	gint		x;
	guint8	*	raw;
	gint32		len;

	raw = cp->len_raw;
	if( ( x = get_octet( decrement ) ) < 0 ) return( x );
	cp->len_len = 1;
	*raw++ = (guint8)x;
	if( x < 128 ) {
		len = x;
	} else {
		x &= 0x7F;
		len = 0;
		switch( x ) {
		default:	return( -3 );		// Length is Larger Than 32 bits
		case 3:
			if( ( x = get_octet( decrement ) ) < 0 ) return( x );
			len |= x;
			len <<= 8;
			*raw++ = (guint8)x;
			cp->len_len += 1;
		case 2:
			if( ( x = get_octet( decrement ) ) < 0 ) return( x );
			len |= x;
			len <<= 8;
			*raw++ = (guint8)x;
			cp->len_len += 1;
		case 1:
			if( ( x = get_octet( decrement ) ) < 0 ) return( x );
			len |= x;
			len <<= 8;
			*raw++ = (guint8)x;
			cp->len_len += 1;
		case 0:
			if( ( x = get_octet( decrement ) ) < 0 ) return( x );
			len |= x;
			*raw++ = (guint8)x;
			cp->len_len += 1;
			break;
		}
	}
	*raw = 0;
	cp->len = len;
	return( len );
}

static	item_t	*	context_stack[ 32 ][ 2 ];

static	gint32	get_identifier( gint32 len )
{
	gint32		z;
	gint		i;
	guint32		code;
	identifier_t	x;
	guint8	*	raw;
	guint8		y;
	guint8		c;	// Class 0 - 4
	gint32		clen;
	gint32		plen;
	cp_t 		cp;

	if( len <= 0 ) return( 0 );

	memset( &cp, 0, sizeof( cp ) );

	code = 0;

	while( len > 0 ) {
		cp.offset = offset;
		raw = cp.id_raw;
		if( ( z = get_octet( &len ) ) < 0 ) return( z );
		*raw++ = *(guint8 *)&x = (guint8)z;
		cp.id_len = 1;
		i = 1;
		c = x.class;
		if( x.code == 0x1F ) {
			code = 0;
			for( i = 2; ; i += 1 ) {
				if( ( z = get_octet( &len ) ) < 0 ) return( z );
				*raw++ = y = (guint8)z;
				cp.id_len += 1;
				if( i == 2 && c == 3 && ( y & 0x40 ) == 0 ) c = 4;	// National
				code |= ( y & 0x7f );
				if( ( y & 0x800 ) == 0 ) break;
				if( code > 0x00FFFFFF ) return( -4 );
				code <<= 7;
			}
		} else {
			code = x.code;
			if( c == 3 ) c = 4;	// National
		}
		*raw = 0;
		cp.class = c;
		cp.id = code;
		if( ( cp.construct = x.construct ) != 0 ) {

			context_stack[ depth ][ PRIMATIVE ] = n_matrix[ CONTEXT ][ PRIMATIVE ];
			context_stack[ depth ][ CONSTRUCT ] = n_matrix[ CONTEXT ][ CONSTRUCT ];

			clen = get_length( &len, &cp );
			print_line( &cp );
			if( clen < 0 ) return( clen );

			depth += 1;
			if( ( z = get_identifier( clen ) ) < 0 ) return( z );
			depth -= 1;

			n_matrix[ CONTEXT ][ CONSTRUCT ] = context_stack[ depth ][ CONSTRUCT ];
			n_matrix[ CONTEXT ][ PRIMATIVE ] = context_stack[ depth ][ PRIMATIVE ];

			len -= clen;

		} else {
			raw = cp.p_raw;
			for( plen = get_length( &len, &cp ); plen > 0; plen -= 1 ) {
				if( ( z = get_octet( &len ) ) < 0 ) return( z );
				*raw++ = y = (guint8)z;
			}
			*raw = 0;
			print_line( &cp );
			if( plen < 0 ) return( plen );
		}
	}
	return( code );
}

#ifdef T1_114_SUBTREE
static  int proto_t1_114_tcap    = -1;
static  int ett_t1_114_tcap      = -1;

static hf_register_info hf[] = {
};

#endif // T1_114_SUBTREE

static	int	dissect_t1_114( tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void * data _U_  )
{
    proto_item* t1_114_item = NULL;

        gint32			len;

//	if( tree == NULL ) return( 0 );

#ifdef T1_114_SUBTREE
        /* create display subtree for the protocol */
        t1_114_item = proto_tree_add_item( tree, proto_t1_114_tcap, tvb, 0, -1, ENC_NA );
        tree = proto_item_add_subtree( t1_114_item, ett_t1_114_tcap );
#endif // T1_114_SUBTREE

        TVB	= tvb;
	PINFO	= pinfo;

	bytes = tvb_get_ptr( TVB, 0, ( len = tvb_reported_length( tvb ) ) );

#ifdef	JUNK
	t1_114_item = proto_tree_add_protocol_format( tree, proto_t1_114, tvb, 0, len, "T1.114: Length: %d", len );
#endif

	col_set_str( PINFO->cinfo, COL_PROTOCOL, "T1.114 TCAP" );

        t1_114_item = proto_tree_add_item( tree, proto_t1_114, tvb, 0, -1, ENC_NA );
        if( global_t1_114_expand ) {
            t1_114_tree = tree;
        } else {
            t1_114_tree = proto_item_add_subtree( t1_114_item, ett_t1_114 );
        }
	depth			= 0;
	offset			= 0;
	remainder		= len;
	n_matrix[ 2 ][ 0 ]	= Context_Primative;	// Default to T1.114
	n_matrix[ 2 ][ 1 ]	= Context_Construct;	// Default to T1.114

	get_identifier( len );

        return len;

#ifdef	JUNK
	proto_tree	*	t1_114_tree = NULL;

        m2pa_item = proto_tree_add_item(tree, proto_m2pa, tvb, 0, -1, ENC_NA);
        m2pa_tree = proto_item_add_subtree(m2pa_item, ett_m2pa);

        //	t1_114_item = proto_tree_add_item( tree, proto_t1_114, tvb, 0, -1, FALSE );

	t1_114_tree = proto_item_add_subtree( t1_114_item, ett_t1_114 );

	if( t1_114_tree == NULL ) return;

	bytes = tvb_get_ptr( TVB, 0, len );

	// Dissect Message Header
	//					Variable,	Buffer,	Offset,	Size,	Start Pointer

	proto_tree_add_bytes(	t1_114_tree,	hf_seq,		tvb,	0,	len,	bytes		);

	return;
#endif
}

void	proto_register_t1_114( void )
{
	if( proto_t1_114 != -1 ) return;

	proto_t1_114 = proto_register_protocol( "T1.114 Transaction Capabilities Application Part", "T1.114", "t1_114" );

	proto_register_field_array( proto_t1_114, hf, array_length( hf ) );

	register_dissector( "t1_114", dissect_t1_114, proto_t1_114 );  // Allow other dissectors to find this one

	proto_register_subtree_array( ett, array_length( ett ) );

	t1_114_module = prefs_register_protocol( proto_t1_114, NULL );

	prefs_register_bool_preference(
		t1_114_module,
		"real_names",
		"Decode Real Names",
		"Whether the real field names or generic TCAP field names will be shown",
		&global_t1_114_names
	);

        prefs_register_bool_preference(
            t1_114_module,
            "expand_tree",
            "Pre-Expanded Protocol List",
            "View pre-expanded TCAP protocl list",
            &global_t1_114_expand
        );

        return;
}

void    proto_reg_handoff_t1_114( void )
{
    t1_114_handle = create_dissector_handle( dissect_t1_114, proto_t1_114 );
    sccp_handle = find_dissector( "sccp" );
}
